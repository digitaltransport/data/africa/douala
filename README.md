# Douala

## SYSTRA

GTFS Dataset produced by [SYSTRA](https://www.systra.com/) as part of a "mobility planning" study funded by [AFD](https://www.afd.fr/).

### GTFS

[**DOWNLOAD GTFS**](https://git.digitaltransport4africa.org/places/douala/raw/master/GTFS-SYSTRA/GTFS-SYSTRA.zip)

#### Info

**Agencies**:
  - Socatur
  - Taxis Co

**Version**: 0 - TimeTable 2018  
**Start**: 2018-01-01  
**End**: 2018-12-31

#### Stats

1. **Main tables**: (\*)

| Table                | Entries           |
| -------------------- | ----------------- |
| agency               | 2 entries         |
| calendar             | 1 entry           |
| stops                | 593 entries       |
| routes               | 68 entries        |
| trips                | 135 entries       |
| stop_times           | 1,254 entries     |

2. **Additional tables**: (\#)

| Table                | Entries           |
| -------------------- | ----------------- |
| calendar_dates       | 0 entry           |
| fare_attributes      | 0 entry           |
| fare_rules           | 0 entry           |
| shapes               | 0 entry           |
| frequencies          | 0 entry           |
| transfers            | 0 entry           |
| feed_info            | 1 entry           |

3. **Other tables**: (^)

| Table                | Entries           |
| -------------------- | ----------------- |
| frequency            | 135 entries       |

\*: required part of GTFS spec, needed to make valid GTFS  
\#: part of GTFS spec but not compulsory  
^: not part of traditional GTFS spec, used by operator for additional   purposes

![](/GTFS-SYSTRA/GTFS-SYSTRA.png)

---

## WhereIsMyTransport

On 18 June 2018, The World Bank contracted [WhereIsMyTransport](https://www.whereismytransport.com/) to supply public transportation data for The World Bank’s Land Use and Transport Accessibility Tool project.

The Douala dataset was collected between ***2 July 2018 to 14 July 2018.*** The World Bank worked with local data collectors who used the WhereIsMyTransport proprietary mobile app to collect data.

2 GTFS Datasets were produced as outputs.

### GTFS #1

[**DOWNLOAD GTFS**](https://git.digitaltransport4africa.org/places/douala/raw/master/GTFS-WIMT/GTFS-WIMT-Yellow_Taxi.zip)

### Info

**Agency**: Yellow Taxis  
**Version**: 1  
**Start**: 2017-09-20  
**End**: 2020-12-31

### Stats

1. **Main tables**: (\*)

| Table                | Entries           |
| -------------------- | ----------------- |
| agency               | 1 entry           |
| calendar             | 2 entry           |
| stops                | 568 entries       |
| routes               | 258 entries       |
| trips                | 516 entries       |
| stop_times           | 1,782 entries     |

2. **Additional tables**: (\#)

| Table                | Entries           |
| -------------------- | ----------------- |
| calendar_dates       | 0 entry           |
| fare_attributes      | 258 entries       |
| fare_rules           | 258 entries       |
| shapes               | 22,786 entries    |
| frequencies          | 3,096 entries     |
| transfers            | 0 entry           |
| feed_info            | 1 entry           |

\*: required part of GTFS spec, needed to make valid GTFS  
\#: part of GTFS spec but not compulsory

![](/GTFS-WIMT/GTFS-WIMT-Yellow_Taxi.png)

### GTFS #2

[**DOWNLOAD GTFS**](https://git.digitaltransport4africa.org/places/douala/raw/master/GTFS-WIMT/GTFS-WIMT-Socatur_Bus.zip)

**Agency**: Socatur Bus  
**Version**: 1  
**Start**: 2017-09-20  
**End**: 2020-12-31

### Stats

1. **Main tables**: (\*)

| Table                | Entries           |
| -------------------- | ----------------- |
| agency               | 1 entry           |
| calendar             | 1 entry           |
| stops                | 110 entries       |
| routes               | 20  entries       |
| trips                | 40  entries       |
| stop_times           | 244   entries     |

2. **Additional tables**: (\#)

| Table                | Entries           |
| -------------------- | ----------------- |
| calendar_dates       | 0 entry           |
| fare_attributes      | 20  entries       |
| fare_rules           | 20  entries       |
| shapes               | 4,830  entries    |
| frequencies          | 0 entry           |
| transfers            | 0 entry           |
| feed_info            | 1 entry           |

\*: required part of GTFS spec, needed to make valid GTFS  
\#: part of GTFS spec but not compulsory

![](/GTFS-WIMT/GTFS-WIMT-Socatur_Bus.png)

### Edited `route_type`
GTFS files wit the suffix `Edited` contains the updated `route_type` property with a value of 700 under `routes.txt`. This is to enable accessability analysis on Conveyal
